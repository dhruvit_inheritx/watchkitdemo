//
//  ItemsViewModel.swift
//  WatchKitDemo
//
//  Created by Dhruvit on 28/03/17.
//  Copyright © 2017 Dhruvit. All rights reserved.
//

import Foundation

struct ItemsViewModel {
    
    private(set) var items : [String] = []
    
    private let kItemsKey = "items"
    private let defaults = UserDefaults(suiteName: "com.inheritx.WatchKitDemo.watchkitapp")
    
    init() {
        items = load()
    }
    
    mutating func append(item : String) {
        items.append(item)
        save(items: items)
    }
    
    mutating func removeItemAt(index : Int) {
        defaults?.set(items, forKey: kItemsKey)
    }
    
    func save(items : [String]) {
        defaults?.set(items, forKey: kItemsKey)
        defaults?.synchronize()
    }
    
    func load() -> [String] {
        return defaults?.value(forKey: kItemsKey) as?  [String] ?? []
    }
}
