//
//  CatalogInterfaceController.swift
//  WatchKitDemo
//
//  Created by Dhruvit on 29/03/17.
//  Copyright © 2017 Dhruvit. All rights reserved.
//

import WatchKit
import Foundation

class CatalogInterfaceController: WKInterfaceController {

    @IBOutlet weak var table: WKInterfaceTable!
    var viewModel = ItemsViewModel()
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
        loadTableData()
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    func loadTableData() {
        let arrItems = viewModel.load()
        print(arrItems.count)
        
        table.setNumberOfRows(arrItems.count, withRowType: "Row")
        
        for (index, item) in arrItems.enumerated() {
            let controller = table.rowController(at: index) as! RowCell
            controller.lblTitle.setText(item)
        }
    }
}
