//
//  LayoutsInterfaceController.swift
//  WatchKitDemo
//
//  Created by Dhruvit on 29/03/17.
//  Copyright © 2017 Dhruvit. All rights reserved.
//

import WatchKit
import Foundation

class RowCell : NSObject {
    @IBOutlet weak var lblTitle : WKInterfaceLabel!
}

class LayoutsInterfaceController: WKInterfaceController {

    @IBOutlet weak var tblView : WKInterfaceTable!
    
    let objects = ["Vertical", "Horizontal", "Mixed",
                   "Pos - Vertical", "Pos - Horizontal","Pos - Centering",
                   "Size - Fit Content","Size - Relative","Size - Fixed", "Size - Fix One",
                   "Order", "Long Scroll"]
    

    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
        
        tblView.setRowTypes(Array(repeating:"Row", count: objects.count))
        
        for (index,obj) in objects.enumerated() {
            let cell = tblView.rowController(at: index) as! RowCell
            cell.lblTitle.setText(obj)
        }
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    override func table(_ table: WKInterfaceTable, didSelectRowAt rowIndex: Int) {
        print("rowIndex :- \(rowIndex)")
        //        pushController(withName: objects[rowIndex], context: nil)
        pushController(withName: objects[rowIndex], context: objects[rowIndex])
    }
}
