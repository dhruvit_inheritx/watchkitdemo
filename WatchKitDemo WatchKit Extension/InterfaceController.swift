//
//  InterfaceController.swift
//  WatchKitDemo WatchKit Extension
//
//  Created by Dhruvit on 27/03/17.
//  Copyright © 2017 Dhruvit. All rights reserved.
//

import WatchKit
import Foundation

class InterfaceController: WKInterfaceController {
    
    @IBOutlet weak var tblView : WKInterfaceTable!

    let objects = ["Layout", "Catalog"]

    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
        
        tblView.setRowTypes(Array(repeating:"Row", count: objects.count))
        
        for (index,obj) in objects.enumerated() {
            let cell = tblView.rowController(at: index) as! RowCell
            cell.lblTitle.setText(obj)
        }
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    override func table(_ table: WKInterfaceTable, didSelectRowAt rowIndex: Int) {
        pushController(withName: objects[rowIndex], context: nil)
    }
    
}
